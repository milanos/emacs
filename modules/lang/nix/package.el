;;; ~/.doom.d/modules/lang/nix/package.el -*- lexical-binding: t; -*-

(package! nix-mode)
(package! company-nixos-options)
(package! nixos-sandbox)
