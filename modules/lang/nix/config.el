;;; ~/.doom.d/modules/lang/nix/config.el -*- lexical-binding: t; -*-


(def-package! nix-mode
  :mode "\\.nix$")

(def-package! company-nixos-options
  :config
  (set-company-backend! 'nix-mode 'company-nixos-options 'company-yasnippet))
